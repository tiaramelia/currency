import Types from '../actions/types';

export default function reducer (state = {
    model: [],
    rates:[],
    initAmount:0,
    listCurrency:[],
    listFilterCurrency:[],
    listCurrName:[
        {name:'Mexican peso', code:'MXN'},
        {name:'Australian dollar', code:'AUD'},
        {name:'Hong Kong dollar', code:'HKD'},
        {name:'Romanian leu', code:'RON'},
        {name:'Croatian kuna', code:'HRK'},
        {name:'Swiss franc', code:'CHF'},
        {name:'Indonesian rupiah', code:'IDR'},
        {name:'Canadian dollar', code:'CAD'},
        {name:'United States dollar', code:'USD'},
        {name:'Japanese yen', code:'JPY'},
        {name:'Brazilian real', code:'BRL'},
        {name:'Philippine peso', code:'PHP'},
        {name:'Czech koruna', code:'CZK'},
        {name:'Norwegian krone', code:'NOK'},
        {name:'Indian rupee', code:'INR'},
        {name:'Polish zloty', code:'PLN'},
        {name:'Icelandic krona', code:'ISK'},
        {name:'Malaysian ringgit', code:'MYR'},
        {name:'South African rand', code:'ZAR'},
        {name:'Israeli new shekel', code:'ILS'},
        {name:'Pound sterling', code:'GBP'},
        {name:'Singapore dollar', code:'SGD'},
        {name:'Hungarian forint', code:'HUF'},
        {name:'European euro', code:'EUR'},
        {name:'Chinese Yuan Renminbi', code:'CNY'},
        {name:'Turkish lira', code:'TRY'},
        {name:'Swedish krona', code:'SEK'},
        {name:'Russian ruble', code:'RUB'},
        {name:'New Zealand dollar', code:'NZD'},
        {name:'South Korean won', code:'KRW'},
        {name:'Thai baht', code:'THB'},
        {name:'Bulgarian lev', code:'BGN'},
        {name:'Danish krone', code:'DKK'},
    ],
    error: null
}, action) {
    switch (action.type) {
        case Types.TYPE_REQUEST_CURRENCY.FETCH_DATA: {
            state.model = action.payload  
            return Object.assign({}, state)
        }
        case Types.TYPE_REQUEST_CURRENCY.FETCH_RATES: {
            let {listCurrName} = state
             // eslint-disable-next-line
            listCurrName.map((row) => {
                row.amount = action.payload[row.code]
            })
             // eslint-disable-next-line
            state.listCurrency = listCurrName.filter((row, index) => row.code != state.model.base).slice(0, 4)

            // eslint-disable-next-line
            state.listFilterCurrency = listCurrName.filter(function (e) {return this.indexOf(e) < 0;}, state.listCurrency).filter((row) => row.code != state.model.base)

            return Object.assign({}, state)
        }
        case Types.TYPE_REQUEST_CURRENCY.CHANGE_INIT_AMOUNT: {
            state.initAmount = action.payload  
            return Object.assign({}, state)
        }
        case Types.TYPE_REQUEST_CURRENCY.SHOW_CURRENCY: {
            let {listFilterCurrency} = state
             // eslint-disable-next-line
            let arrayCurrency = listFilterCurrency.filter((row) => row.code == action.payload)

            // eslint-disable-next-line
            state.listFilterCurrency = listFilterCurrency.filter((row) => row.code != action.payload)
            state.listCurrency = state.listCurrency.concat(arrayCurrency)
            
            return Object.assign({}, state)
        }
        case Types.TYPE_REQUEST_CURRENCY.DELETE_CURRENCY: {
            let {listCurrency} = state

            // eslint-disable-next-line
            let arrayCurrency = listCurrency.filter((row) => row.code == action.payload)
            // eslint-disable-next-line
            state.listCurrency = listCurrency.filter((row) => row.code != action.payload)
            state.listFilterCurrency = state.listFilterCurrency.concat(arrayCurrency)
            
            return Object.assign({}, state)
        }
        case Types.TYPE_REQUEST_CURRENCY.REJECTED_CURRENCY: {
            state.error = action.payload
            return Object.assign({}, state)
        }
        default:
            return state
    }
}
import Types from './types'
import AppApi from '../api'

export function FetchDataCurrency (code)
{
    return function (dispatch)
    {
        AppApi.currency.fetchDataCurrency(code).then((rates) => {
            dispatch({type:Types.TYPE_REQUEST_CURRENCY.FETCH_DATA, payload:rates});
            dispatch({type:Types.TYPE_REQUEST_CURRENCY.FETCH_RATES, payload:rates.rates});
        }).catch((res) => {
            dispatch({type:Types.TYPE_REQUEST_CURRENCY.REJECTED_CURRENCY, payload:res});
        });
    }
}

export function ChangeInitValue(value) {
    return function(dispatch) {
        dispatch({
            type: Types.TYPE_REQUEST_CURRENCY.CHANGE_INIT_AMOUNT,
            payload: value
        })
    }
}

export function ShowCurrency(value) {
    return function(dispatch) {
        dispatch({
            type: Types.TYPE_REQUEST_CURRENCY.SHOW_CURRENCY,
            payload: value
        })
    }
}

export function DeleteCurrency(value) {
    return function(dispatch) {
        dispatch({
            type: Types.TYPE_REQUEST_CURRENCY.DELETE_CURRENCY,
            payload: value
        })
    }
}